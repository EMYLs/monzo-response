from django.urls import path
from . import views

urlpatterns = [
    path("register", views.register, name="register"),
    path("login/<int:id>/", views.loginwithId, name="login"),
]