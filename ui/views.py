from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse, Http404

from core.models import Incident
from slack.models import PinnedMessage, UserStats

from django.contrib import messages
from django.contrib.auth.models import User, auth


def incident_doc(request: HttpRequest, incident_id: str):
    print('______________________________________________________ damn ', )
    try:
        incident = Incident.objects.get(pk=incident_id)
    except Incident.DoesNotExist:
        raise Http404("Incident does not exist")

    events = PinnedMessage.objects.filter(incident=incident).order_by('timestamp')
    user_stats = UserStats.objects.filter(incident=incident).order_by('-message_count')[:5]

    if request.user.is_authenticated:  # <-  no parentheses any more!
        print('______________________________________________________ damn')

    # else:
    #     return render('login', template_name='login.html', context={
    #         "incident": incident,
    #         "events": events,
    #         "user_stats": user_stats,
    #     })
    # do something if the user is authenticated

    # return render('login')

    return render(request, template_name='incident_doc.html', context={
        "incident": incident,
        "events": events,
        "user_stats": user_stats,
    })