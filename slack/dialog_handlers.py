import json
from datetime import datetime
from django.conf import settings
from django.contrib.auth.models import User, auth
from django.shortcuts import render, redirect
from urllib.parse import urljoin
from django.urls import reverse
from django.template import loader
from django.conf import settings

from slack.settings import INCIDENT_EDIT_DIALOG, INCIDENT_REPORT_DIALOG, INCIDENT_DOCUMENT_LOGIN
from core.models.incident import Incident, ExternalUser
from slack.models import HeadlinePost, CommsChannel
from slack.decorators import dialog_handler
from slack.slack_utils import send_ephemeral_message, channel_reference, get_user_profile, GetOrCreateSlackExternalUser
from slack.models import PinnedMessage, UserStats

from django.http import HttpRequest, HttpResponse, Http404

import logging
logger = logging.getLogger(__name__)


@dialog_handler(INCIDENT_REPORT_DIALOG)
def report_incident(user_id: str, channel_id: str, submission: json, response_url: str, state: json):
    report = submission['report']
    summary = submission['summary']
    impact = submission['impact']
    lead_id = submission['lead']
    severity = submission['severity']

    name = get_user_profile(user_id)['name']
    reporter = GetOrCreateSlackExternalUser(external_id=user_id, display_name=name)

    lead = None
    if lead_id:
        lead_name = get_user_profile(lead_id)['name']
        lead = GetOrCreateSlackExternalUser(external_id=lead_id, display_name=lead_name)

    Incident.objects.create_incident(
        report=report,
        reporter=reporter,
        report_time=datetime.now(),
        summary=summary,
        impact=impact,
        lead=lead,
        severity=severity,
    )

    incidents_channel_ref = channel_reference(settings.INCIDENT_CHANNEL_ID)
    text = f"Thanks for raising the incident 🙏\n\nHead over to {incidents_channel_ref} to complete the report and/or help deal with the issue"
    send_ephemeral_message(channel_id, user_id, text)


@dialog_handler(INCIDENT_EDIT_DIALOG)
def edit_incident(user_id: str, channel_id: str, submission: json, response_url: str, state: json):
    report = submission['report']
    summary = submission['summary']
    impact = submission['impact']
    lead_id = submission['lead']
    severity = submission['severity']

    lead = None
    if lead_id:
        lead_name = get_user_profile(lead_id)['name']
        lead = GetOrCreateSlackExternalUser(external_id=lead_id, display_name=lead_name)

    try:
        incident = Incident.objects.get(pk=state)

        # deliberately update in this way the post_save signal gets sent
        # (required for the headline post to auto update)
        incident.report = report
        incident.summary = summary
        incident.impact = impact
        incident.lead = lead
        incident.severity = severity
        incident.save()

    except Incident.DoesNotExist:
        logger.error(f"No incident found for pk {state}")



@dialog_handler(INCIDENT_DOCUMENT_LOGIN)
def document_login(user_id: str, channel_id: str, submission: json, response_url: str, state: json):

    username = submission['username']
    password = submission['password']

    user = auth.authenticate(username=username, password=password)
    print("_________________________________________________user_id      : " , user_id)
    print("_________________________________________________channel_id   : " , channel_id)
    print("_________________________________________________submission   : " , submission)
    print("_________________________________________________response_url : " , response_url)
    print("_________________________________________________state        : " , state)

    if user is not None:
        # auth.login(request, user)

        doc_url = urljoin(
            settings.SITE_URL,
            reverse('incident_doc', kwargs={'incident_id': 8})
        )

        incident_id = 9
        try:
            incident = Incident.objects.get(pk=incident_id)
        except Incident.DoesNotExist:
            raise Http404("Incident does not exist")

        events = PinnedMessage.objects.filter(incident=incident).order_by('timestamp')
        user_stats = UserStats.objects.filter(incident=incident).order_by('-message_count')[:5]

        # tem = get_template('incident_doc', using=None)
        print("_________________________________________________authed        : ")
        # return render(doc_url, 'incident_doc.html', {})
        return render(doc_url, template_name='incident_doc.html', context={
            "incident": incident,
            "events": events,
            "user_stats": user_stats,
        })



        # print("_________________________________________________auth        : ")
        # return redirect("/incident/1/", '8')
        # # return render('incident/8/', '8')
        # # return render(request, template_name='incident_doc.html', context={
        # #     "incident": incident,
        # #     "events": events,
        # #     "user_stats": user_stats,
        # # })

    else:
        print("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee")